## Changelog

#### 1.0.1
* testing updating with GitHub Updater
* moved release notes

#### 1.0
* theme now managed by Bitbucket repository and updated via GitHub Updater plugin
* unified with Family2010

####0.2
* comments.php is now using comment_form() instead of a form block to support Jetpack's comment system
* split apart post creation and loop for outputting posts from index.php into post-loop.php for supporting Jetpack infinite scroll
* separated  header and footer code from index.php into header.php and footer.php respectively
* separated out sidebar code from index.php into sidebar.php
* added add_theme_support() code to functions.php to enable Jetpack infinite scroll
* added infinite_scroll_render() function to functions.php to be the render code for new posts as users infinitely scroll
* added infinite_scroll_credit() function as a filter to functions.php to customize the credits displayed in the footer used when infinitely scrolling based on a family2013 setting or using the default credits if the setting is empty
* added footer credits option in theme-options.php that allows admins to customize the infinite scroll footer without editing the functions.php, though haven't gotten it to properly save and retrieve html this way
* added <?php body_class(); ?> to the <body> tag in index.php
* wrapped showing the navigation div into checking first if either infinite scroll is deactivated or Javascript disabled, the only two instances when this div is necessary anymore

####0.1
* initial release