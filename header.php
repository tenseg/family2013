<!DOCTYPE html>
<html <?php language_attributes();?>>
<! Head element -->
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' );?>; charset=<?php bloginfo( 'charset' );?>" />
<title><?php bloginfo( 'name' );?> <?php if ( is_single() ) {?> &raquo; Blog Archive <?php }?> <?php wp_title();?></title>
<meta name="description" content="<?php bloginfo( 'description' );?>">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' );?> RSS Feed" href="<?php bloginfo( 'rss2_url' );?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' );?>" />
<?php wp_head();?>
</head>
<! Start body -->
<body <?php body_class();?>>
<div id="masthead">
<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>
         <div class='pagegroup'><a href="<?php
// include the alternate group url if present
if ( family2013option( 'groupurl' ) ) {
	echo family2013option( 'groupurl' );
} else {
	echo get_option( 'home' );
}
?>/" ><?php
// include the alternate group text if present
if ( family2013option( 'groupname' ) ) {
	echo family2013option( 'groupname' );
} else {
	bloginfo( 'name' );
}
?></a></div>
<div class='pagetitle'><?php
if ( is_404() ) { /* If this is a 404 page */
	?>Not Found<?php
} elseif ( is_category() ) { /* If this is a category archive */
	?>Updates on <?php single_cat_title( '' );
} elseif ( is_day() ) { /* If this is a yearly archive */
	?>Archives for <?php the_time( 'l, j F Y' );
} elseif ( is_month() ) { /* If this is a monthly archive */
	?>Archives for <?php the_time( 'F Y' );
} elseif ( is_year() ) { /* If this is a yearly archive */
	?>Archives for <?php the_time( 'Y' );
} elseif ( is_search() ) { /* If this is a search archive */
	?>Search for '<?php the_search_query();?>'<?php
} elseif ( is_single() || is_page() ) {
	the_title();
} else {
	?>&nbsp;<?php
}
?></div>
</td></tr></table>
</div>
