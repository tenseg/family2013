<!--PageFooterFmt-->
<div id="footer">
<table border="0" width="100%"><tr><td align=left>
	<?php if ( is_page() ) {?>
		<p>
		<?php
global $user_level;
	get_currentuserinfo();
	if ( $user_level > 2 ) {
		echo '<a class="noprint" href="';
		echo get_edit_post_link( $post->ID );
		echo '" accesskey="e">Edit this page.</a>';
	} else {
		echo '<a class="noprint" href="';
		echo wp_login_url( get_edit_post_link( $post->ID ) );
		echo '" accesskey="e">&nbsp;</a>';
	}
	?></p><?php
} // is page ?>
</td><td align=right>
<p>
 <?php
echo infinite_scroll_credit();
?>
</p>
</td></tr></table>
</div> <!-- end footer -->
<!--/PageFooterFmt-->
<?php wp_footer();?>
</body>
</html>