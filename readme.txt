=== Family2013 Theme ===
Contributors: Alexander Celeste and Eric Celeste
Tags: wiki
Requires at least: 3.5
Tested up to: 4.2
Stable tag: master
License: The MIT License
License URI: http://opensource.org/licenses/MIT
Bitbucket Theme URI: https://bitbucket.org/tenseg/family2013
Bitbucket Branch: master

This is a clean theme based on the Celeste Family Wiki.

== Description ==

A theme designed to match the theme used on the Celeste Family Wiki. Regular site visitors will barely see a difference between wiki and wp.

**Features**

- Customizable color scheme
- Full support for Jetpack features like infinite scroll and comments
- Customizable footer credit and group text 
- Widgetized right sidebar area

**Notes**

This theme is really designed only for use by Celeste and Hess family members who have Wiki pages and want their WordPress sites to match in design. It is not completely designed for wider public use.

== Installation ==

The easiest way to install and maintain this theme is with [GitHub Updater](https://github.com/afragen/github-updater). The great thing about using GitHub Updater is that the theme will then be able to be updated through the regular WordPress update process.

Of course, if you wish to install and update the theme manually, you are welcome to do so.

**Installing with GitHub Updater**

1. First make sure you have [GitHub Updater](https://github.com/afragen/github-updater) itself installed.
2. In the WordPress Dashboard go to *Settings > GitHub Updater > Install Theme*.
3. Enter `tenseg/family2013` as the Theme URI and make sure the Remote Repository Host is `Bitbucket`. Then click the "Install Theme".
4. Activate the Family2013 theme after it has installed.

When the theme is updated, GitHub Updater will make sure that WordPress knows this and presents the usual offers to update the theme for you.

**Installing manually**

1. Download the [master.zip](https://bitbucket.org/tenseg/family2013/get/master.zip) file from the Family2013 repository.
2. This file will have an odd name. Rename it to `family2013.zip`.
3. In the WordPress Dashboard go to *Themes > Add New* and click on the small "this page" link offering to upload a plugin in .zip format.
4. Use the "Choose File" button and choose the "family2013.zip" file you just renamed.
5. Activate the Family2013 theme after it has installed.

When the theme is updated, you will have to go through this same process to download and install the new version.

== Changelog ==

#### 1.0.1
* testing updating with GitHub Updater

#### 1.0
* theme now managed by Bitbucket repository and updated via GitHub Updater plugin
* unified with Family2010

####0.2
* comments.php is now using comment_form() instead of a form block to support Jetpack's comment system
* split apart post creation and loop for outputting posts from index.php into post-loop.php for supporting Jetpack infinite scroll
* separated  header and footer code from index.php into header.php and footer.php respectively
* separated out sidebar code from index.php into sidebar.php
* added add_theme_support() code to functions.php to enable Jetpack infinite scroll
* added infinite_scroll_render() function to functions.php to be the render code for new posts as users infinitely scroll
* added infinite_scroll_credit() function as a filter to functions.php to customize the credits displayed in the footer used when infinitely scrolling based on a family2013 setting or using the default credits if the setting is empty
* added footer credits option in theme-options.php that allows admins to customize the infinite scroll footer without editing the functions.php, though haven't gotten it to properly save and retrieve html this way
* added <?php body_class(); ?> to the <body> tag in index.php
* wrapped showing the navigation div into checking first if either infinite scroll is deactivated or Javascript disabled, the only two instances when this div is necessary anymore

####0.1
* initial release