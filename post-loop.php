<?php while ( have_posts() ) {the_post();?>
	<div <?php post_class()?> id="post-<?php the_ID();?>">
		<?php if ( !is_page() && !is_single() ) {?>
			<p style="margin-bottom:-5px;"><small>
			<?php the_time( 'j F Y' );?>
			<?php if ( !is_single() ): ?> .
				<?php comments_popup_link( 'Reply', '1 Reply', '% Replies' );?>
				<?php edit_post_link( 'Edit', ' . ', '' );?>
			<?php endif;?>
			</small></p>
			<h2 style="margin-top:0px;"><a href="<?php the_permalink()?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute();?>"><?php the_title();?></a></h2>
		<?php } /* if not single or page */?>

		<div class="entry">
			<?php the_content( 'Read the rest of this entry &raquo;' );?>
		</div>

		<?php if ( is_single() ) {?>
			<div id="postmeta">
				<p>
					<?php
global $user_level;
	wp_get_current_user();
	if ( $user_level > 2 ) {
		echo '<a class="noprint" href="';
		echo ( get_edit_post_link( $post->ID ) );
		echo '" accesskey="e">Edit this entry.</a>';
	}
	?>
					Posted on <?php echo get_the_time( 'l, j F Y', $post->ID ); ?> at <?php echo get_the_time( '', $post->ID ); ?>
					and filed under
					<?php the_category( ', ', '', $post->ID )?>.
				</p>
			</div>
			<?php comments_template();?>
		<?php } /* if single */?>
	</div>
<?php } /* while have_posts */?>