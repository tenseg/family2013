<?php
// found at http://themeshaper.com/sample-theme-options/

add_action( 'admin_menu', 'family2013_theme_options' );

/**
 * Load up the menu page
 */
function family2013_theme_options() {
	add_theme_page( __( 'Family2013 Options' ), __( 'Family2013 Options' ), 'edit_theme_options', 'family2013_theme_options', 'family2013_handler' );
}

/**
 * Create arrays for our select and radio options
 */
$family2013_color_options = [
	'0' => [
		'value' => 'purple',
		'label' => __( 'Purple' ),
	],
	'1' => [
		'value' => 'blue',
		'label' => __( 'Blue' ),
	],
	'2' => [
		'value' => 'teal',
		'label' => __( 'Teal' ),
	],
	'3' => [
		'value' => 'green',
		'label' => __( 'Green' ),
	],
	'4' => [
		'value' => 'darkgreen',
		'label' => __( 'Dark Green' ),
	],
	'5' => [
		'value' => 'yellow',
		'label' => __( 'Yellow' ),
	],
	'6' => [
		'value' => 'orange',
		'label' => __( 'Orange' ),
	],
	'7' => [
		'value' => 'red',
		'label' => __( 'Red' ),
	],
];

/**
 * Create the options page
 */
function family2013_handler() {
	global $family2013_color_options;

	if ( !current_user_can( 'edit_theme_options' ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$options = get_option( 'family2013_options' );

	if ( !$options ) { // if the customizations don't exist yet
		$defaults = []; // create an empty array
		$defaults['color'] = 'blue'; // set the default theme color
		$defaults['sidebarurl'] = ''; // set the default include sidebar url
		$defaults['groupname'] = ''; // set the default group text
		$defaults['groupurl'] = ''; // set the default group url
		$defaults['footercredits'] = ''; // set the default infinite scroll footer credits

		add_option( 'family2013_options', $defaults ); // add our customizations to the database
	}

	if ( isset( $_POST['submit'] ) ) { // If they've clicked the submit button
		if ( isset( $_POST['family2013color'] ) ) {
			$options['color'] = htmlentities( stripslashes( $_POST['family2013color'] ) ); // escape and save the new background color
		}
		if ( isset( $_POST['family2013groupname'] ) ) {
			$options['groupname'] = htmlentities( stripslashes( $_POST['family2013groupname'] ) ); // escape and save the new url
		}
		if ( isset( $_POST['family2013groupurl'] ) ) {
			$options['groupurl'] = htmlentities( stripslashes( $_POST['family2013groupurl'] ) ); // escape and save the new url
		}
		if ( isset( $_POST['family2013sidebarurl'] ) ) {
			$options['sidebarurl'] = htmlentities( stripslashes( $_POST['family2013sidebarurl'] ) ); // escape and save the new url
		}
		if ( isset( $_POST['family2013footercredits'] ) ) {
			$options['footercredits'] = htmlentities( stripslashes( $_POST['family2013footercredits'] ) ); // save the new footer credits
		}
		update_option( 'family2013_options', $options ); // update the database

		$url = get_bloginfo( 'url' ); // get the blog url from the database
		$updated_html = "
			<div id='message' class='updated below-h2'>
				<p>Theme updated. <a href='{$url}'>Visit your site</a> to see how it looks.</p>
			</div>
		"; // some HTML to show that the options were updated
	}

	?>
		<div class="wrap">
			<div id="icon-themes" class="icon32"><br /></div>
			<h2>Family2013 Theme Options</h2>
			<?php if ( isset( $updated_html ) ) {echo $updated_html;}?>

			<form action="" method="post" id="family2013-form">
				<table class="form-table">
					<tr>
						<td colspan="2">
							<h3>Colors</h3><p>The Family2013 theme has a few alternative color schemes. If you want a set of colors that are not already included, please <a href="mailto:efc@clst.org">contact me</a> and we'll see what we can do!</p>
						</td>
					</tr>
					<tr>
						<th scope="row" valign="top"><label for="color">Color Scheme: </label></th>
						<td>
							<select name="family2013color">
							<?php
$selected = $options['color'];
	$p = '';
	$r = '';

	foreach ( $family2013_color_options as $option ) {
		$label = $option['label'];
		if ( $selected == $option['value'] ) { // Make default first in list
			$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
		} else {
			$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
		}

	}
	echo $p . $r;
	?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<h3>Group Text</h3><p>The Family2013 theme includes a "group name" that hovers behind the blog title. By default the name used will be the description of the blog from the general blog settings and clicking on this text will just go back to the blog home. However, you can override both the text used and the destination URL below. Remember that the theme will automatically force this text into lower case.</p>
						</td>
					</tr>
					<tr>
						<th scope="row" valign="top"><label for="sidebarurl">Group text: </label></th>
						<td>
							<input type="text" name="family2013groupname" value="<?php echo family2013option( 'groupname' ) ?>" size="50">
						</td>
					</tr>
					<tr>
						<th scope="row" valign="top"><label for="sidebarurl">Group URL: </label></th>
						<td>
							<input type="text" name="family2013groupurl" value="<?php echo family2013option( 'groupurl' ) ?>" size="50"><br />(don't forget the "http://")
						</td>
					</tr>
										<tr>
						<td colspan="2">
							<h3>Footer Credits</h3><p>The Family2013 theme allows you to set the text used as the footer credits.</p>
						</td>
					</tr>
					<tr>
						<th scope="row" valign="top"><label for="sidebarurl">Group text: </label></th>
						<td>
							<input type="text" name="family2013footercredits" value="<?php echo family2013option( 'footercredits' ) ?>" size="50">
						</td>
					</tr>
                </table>

				<br /><span class='submit' style='border: 0;'><input type='submit' name='submit' value='Save Options' /></span>
			</form>
		</div>
<?php
}

if ( get_option( 'family2013_options' ) ) { // if the customizations exist
	function family2013_header_addition() { // display function for the customizations
		$options = get_option( 'family2013_options' ); // get our customizations from the database

		if ( $options['color'] ) {
			?>
<link rel="stylesheet" href="<?php echo get_bloginfo( 'template_url' ); ?>/family2013color-<?php echo $options['color']; ?>.css" type="text/css" />
<?php
}
	}
	add_action( 'wp_head', 'family2013_header_addition' ); // attach our display function to the wp_head function
}