# Family2013 Theme

- Contributors: Alexander Celeste and Eric Celeste
- Tags: wiki
- Requires at least: 3.5
- Tested up to: 4.1.1
- Stable tag: 1.0.1
- License: The MIT License
- License URI: http://opensource.org/licenses/MIT

This is a clean theme based on the Celeste Family Wiki.

## Description

A theme designed to match the theme used on the Celeste Family Wiki. Regular site visitors will barely see a difference between wiki and wp.

## Features

- Customizable color scheme

- Full support for Jetpack features like infinite scroll and comments

- Customizable footer credit and group text 

- Widgetized right sidebar area

## Installation

The easiest way to install and maintain this theme is with [GitHub Updater](https://github.com/afragen/github-updater). The great thing about using GitHub Updater is that the theme will then be able to be updated through the regular WordPress update process.

Of course, if you wish to install and update the theme manually, you are welcome to do so.

### Installing with GitHub Updater
1. First make sure you have [GitHub Updater](https://github.com/afragen/github-updater) itself installed.
2. In the WordPress Dashboard go to *Settings > GitHub Updater > Install Theme*.
3. Enter `tenseg/family2013` as the Theme URI and make sure the Remote Repository Host is `Bitbucket`. Then click the "Install Theme".
4. Activate the Family2013 theme after it has installed.

When the theme is updated, GitHub Updater will make sure that WordPress knows this and presents the usual offers to update the theme for you.

### Installing manually
1. Download the [master.zip](https://bitbucket.org/tenseg/family2013/get/master.zip) file from the Family2013 repository.
2. This file will have an odd name. Rename it to `family2013.zip`.
3. In the WordPress Dashboard go to *Themes > Add New* and click on the small "this page" link offering to upload a plugin in .zip format.
4. Use the "Choose File" button and choose the "family2013.zip" file you just renamed.
5. Activate the Family2013 theme after it has installed.

When the theme is updated, you will have to go through this same process to download and install the new version.

## Notes

This theme is really designed only for use by Celeste and Hess family members who have Wiki pages and want their WordPress sites to match in design. It is not completely designed for wider public use.