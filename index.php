<!DOCTYPE html>
<?php
// get the header and start the <body> from header.php
get_header();
?>
<! Start sidebar -->
<?php
// load the sidebar from sidebar.php
get_sidebar();
?>
<!-- end sidebar -->

<! Start main page content -->
<div id="wikitext" role="main">

	<?php if ( is_search() ): ?>
		<p><?php get_search_form();?><br /></p>
	<?php endif;?>

	<?php if ( have_posts() ):
	// loop to get posts from the database and display them
	get_template_part( 'post-loop' );
	?>

	<! Show navigation buttons only if infinite scroll itself deactivated-->
	<?php
	//Get Jetpack enabled modules, this is an array
	$jetpack_options = get_option( 'jetpack_active_modules' );
	$efcDoSearch = FALSE;
//If infinite scroll isn't enabled: We're checking the array of enabled Jetpack modules to see if 'infinite-scroll' is a value, if it isn't...
	if ( is_array($jetpack_options) && in_array( 'infinite-scroll', $jetpack_options ) == false || is_search() ) {
		//Add the navigation div
		?>
		<div class="navigation">
			<p>
				<?php next_posts_link( '&laquo; Older' )?>
				<?php previous_posts_link( 'Newer &raquo;' )?>
			</p>
		</div>
		<?php
	}
	?>
		<?php else: ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php $efcDoSearch = TRUE;?>

	<?php endif;?>

	<?php if ( is_page( 'search' ) || $efcDoSearch ): ?>
		<?php get_search_form();?>
	<?php endif;?>

<!--PageText-->
<!--PageRightFmt-->
</div> <!-- end wikitext -->
<?php
// get the page footer and end the <body> from footer.php
get_footer();
?>